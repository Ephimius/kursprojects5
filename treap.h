#ifndef TREAP_TREAP_H
#define TREAP_TREAP_H

#include <chrono>
#include <random>
#include <utility>
#include <vector>

template <class T, class F>
class Treap {
private:
    class Node {
    private:
        std::mt19937* rnd;
        int priority;
        size_t siz;
        T val;
        Node* ll;
        Node* rr;
        bool rev;
        T buffv;
        F func;
        bool modif;

        void ModifyUpdater(T modificator) {
            if (modif) {
                buffv = func(buffv, modificator);
            } else {
                modif = true;
                buffv = modificator;
            }
        }

        void Update() {
            if (rev) {
                rev = false;
                std::swap(ll, rr);
                if (ll != nullptr) {
                    ll->rev = (1 ^ ll->rev);
                }
                if (rr != nullptr) {
                    rr->rev = (1 ^ rr->rev);
                }
            }
            if (modif) {
                modif = false;
                if (ll != nullptr) {
                    ll->ModifyUpdater(buffv);
                }
                if (rr != nullptr) {
                    rr->ModifyUpdater(buffv);
                }
                val = func(val, buffv);
            }
        }

        void UpdateSiz() {
            siz = 1;
            if (ll != nullptr) {
                siz += ll->siz;
            }
            if (rr != nullptr) {
                siz += rr->siz;
            }
        }

    public:
        Node(T value, std::mt19937* random, F funct) :
                rnd(random), priority((*rnd)() & 2147483648), siz(1), val(value), ll(nullptr),
                rr(nullptr), rev(false), func(funct), modif(false) {
        }

        int LowerBound(T value) {
            int ans;
            Update();
            if (val < value) {
                ans = (ll == nullptr ? 0 : ll->siz);
                if (rr == nullptr) {
                    return ans;
                }
                return ans + rr->LowerBound(value) + 1;
            }
            if (ll == nullptr) {
                return -1;
            }
            return ll->LowerBound(value);
        }

        static void Reverse(Node*& t_, int l, int r) {
            t_->Update();
            if (r <= l) {
                return;
            }
            Node* tl = nullptr;
            Node* tm = nullptr;
            Node* tr = nullptr;
            Split(t_, t_, tr, r);
            Split(t_, tl, tm, l);
            if (tm != nullptr) {
                tm->rev = (1 ^ tm->rev);
            }
            Merge(t_, tl, tm);
            Merge(t_, t_, tr);
        }

        static void Modify(Node*& t_, int l, int r, T modif) {
            t_->Update();
            if (r <= l) {
                return;
            }
            Node* tl = nullptr;
            Node* tm = nullptr;
            Node* tr = nullptr;
            Split(t_, t_, tr, r);
            Split(t_, tl, tm, l);
            if (tm != nullptr) {
                tm->ModifyUpdater(modif);
            }
            Merge(t_, tl, tm);
            Merge(t_, t_, tr);
        }

        static void Insert(Node*& t_, Node* xx) {
            Node* tt = nullptr;
            Node* tt_ = nullptr;
            t_->Out();
            Split(t_, tt, tt_, t_->LowerBound(xx->val) + 1);
            if (tt) tt->Out();
            if (tt_) tt_->Out();
            Merge(t_, tt, xx);
            t_->Out();
            Merge(t_, t_, tt_);
            t_->Out();
        }

        static void Remove(Node*& t_, T xx) {
            Node* tt = nullptr;
            Node* tt_ = nullptr;
            Node* tt__ = nullptr;
            Split(t_, tt, tt_, t_->LowerBound(xx) + 1);
            if (tt) tt->Out();
            Split(tt_, tt_, tt__, 1);
            delete tt_;
            if (tt__) tt__->Out();
            Merge(t_, tt, tt__);
        }

        T* Get(size_t ind) {
            Update();
            if (ind == (ll == nullptr ? 0 : ll->siz)) {
                return &val;
            }
            if (ind < (ll == nullptr ? 0 : ll->siz)) {
                if (ll == nullptr) {
                    return nullptr;
                }
                return ll->Get(ind);
            }
            if (rr == nullptr) {
                return nullptr;
            }
            return rr->Get(ind - (ll == nullptr ? 0 : ll->siz) - 1);
        }

        static void Merge(Node*& rez, Node* ll, Node* rr) {
            if (ll != nullptr) {
                ll->Update();
            }
            if (rr != nullptr) {
                rr->Update();
            }
            if (ll == nullptr || rr == nullptr) {
                rez = (rr == nullptr ? ll : rr);
            } else if (ll->priority < rr->priority) {
                Merge(rr->ll, ll, rr->ll);
                rez = rr;
            } else {
                Merge(ll->rr, ll->rr, rr);
                rez = ll;
            }
            rez->UpdateSiz();
        }

        static void Split(Node* what, Node*& ll, Node*& rr, int ls) {
            if (what == nullptr) {
                ll = nullptr;
                rr = nullptr;
                return;
            }
            what->Update();
            if ((what->ll != nullptr ? what->ll->siz : 0) >= ls) {
                Split(what->ll, ll, what->ll, ls);
                rr = what;
            } else {
                Split(what->rr, what->rr, rr, ls - (what->ll != nullptr ? what->ll->siz : 0) - 1);
                ll = what;
            }
            what->UpdateSiz();
        }

        ~Node() {
            delete ll;
            delete rr;
        }
    };

    Node* root;
    F func;
    std::mt19937 rnd;
    size_t size;

public:
    Treap(const std::vector<T>& data, F funct, bool random = true, int seed = 0) :
            root(nullptr), func(funct), size(data.size()),
            rnd(random ? (unsigned)std::chrono::steady_clock::now().time_since_epoch().count() : seed) {
        if (size > 0) {
            root = new Node(data[0], &rnd, func);
        }
        for (size_t i = 1; i < size; ++i) {
            Node* t = new Node(data[i], &rnd, func);
            root->Merge(root, root, t);
        }
    }

    Treap(const Treap& treap) = delete;

    Treap(Treap&& treap) : root(treap.root), func(treap.func), rnd(treap.rnd), size(treap.size) {
        treap.root = nullptr;
        treap.size = 0;
    }

    Treap& operator=(const Treap& treap) = delete;

    Treap& operator=(Treap&& treap) {
        delete root;
        root = treap.root;
        func = treap.func;
        rnd = treap.rnd;
        size = treap.size;
        treap.root = nullptr;
        treap.size = 0;
    }

    void Reverse(size_t l, size_t r) {
        l = std::max(l, size_t(0));
        r = std::min(r, size);
        if (l >= r) {
            return;
        }
        if (root != nullptr) {
            Node::Reverse(root, l, r);
        }
    }

    void Modify(T modif, size_t l, size_t r) {
        l = std::max(l, size_t(0));
        r = std::min(r, size);
        if (l >= r) {
            return;
        }
        if (root != nullptr) {
            Node::Modify(root, l, r, modif);
        }
    }

    T GetAt(size_t ind) {
        T* rez = root->Get(ind);
        if (rez == nullptr) {
            throw std::length_error("Outside of memory");
        }
        return T(*rez);
    }

    ~Treap() {
        delete root;
    }
};

#endif //TREAP_TREAP_H