#ifndef TREAP_TESTER_H
#define TREAP_TESTER_H

#include <algorithm>

#include "treap.h"
#include "generator.h"

template <class F>
int Testing(std::vector<int> data, std::vector<Test> que, F func, int bas) {
    //Treap<int, F> treap(data, func, false);
    Treap<int, F> treap0(data, func);
    Treap<int, F> treap1(std::vector<int>(), func);
    treap1 = std::move(treap0);
    Treap<int, F> treap(std::move(treap1));
    for (int i = 0; i < que.size(); ++i) {
        /*if (i % 333 == 0) {
            std::cout << "l" << que.size() - i << " ";
        }*/
        if (que[i].type == Que::REVERSE) {
            treap.Reverse(que[i].l, que[i].r);
            std::reverse(data.begin() + que[i].l, data.begin() + que[i].r);
        } else if (que[i].type == Que::MODIFY) {
            treap.Modify(que[i].v, que[i].l, que[i].r);
            for (int j = que[i].l; j < que[i].r; ++j) {
                data[j] = func(data[j], que[i].v);
            }
        } else {
            if (treap.GetAt(que[i].l) != data[que[i].l]) {
                return i;
            }
        }
    }
    for (int i = 0; i < data.size(); ++i) {
        if (treap.GetAt(i) != data[i]) {
            return que.size();
        }
    }
    return -1;
}

#endif //TREAP_TESTER_H
