#ifndef TREAP_LIBRARY_H
#define TREAP_LIBRARY_H

#include <chrono>
#include <random>
#include <utility>
#include <vector>

template <class T, class F>
class Treap {
private:
    class Node {
    private:
        std::mt19937* rnd;
        int priority;
        size_t siz;
        T val;
        Node* ll;
        Node* rr;
        bool rev;
        T buffv;
        F func;
        bool modif;

        void ModifyUpdater(T modificator);

        void Update();

        void UpdateSiz();

    public:
        Node(T value, std::mt19937* random, F funct);

        static void Reverse(Node*& t_, int l, int r);

        static void Modify(Node*& t_, int l, int r, T modif);

        T* Get(size_t ind);

        static void Merge(Node*& rez, Node* ll, Node* rr);

        static void Split(Node* what, Node*& ll, Node*& rr, int ls);

        ~Node() {
            delete ll;
            delete rr;
        }
    };

    Node* root;
    F func;
    std::mt19937 rnd;
    size_t size;

public:
    Treap(const std::vector<T>& data, F funct, bool random = true, int seed = 0);

    Treap(const Treap& treap) = delete;

    Treap(Treap&& treap);

    Treap& operator=(const Treap& treap) = delete;

    Treap& operator=(Treap&& treap);

    void Reverse(size_t l, size_t r);

    void Modify(T modif, size_t l, size_t r);

    T GetAt(size_t ind);

    ~Treap() {
        delete root;
    }
};

#endif //TREAP_LIBRARY_H