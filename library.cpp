#include "library.h"

#include <chrono>
#include <random>
#include <stdexcept>
#include <utility>
#include <vector>

template <class T, class F>
void Treap<T, F>::Node::ModifyUpdater(T modificator) {
    if (modif) {
        buffv = func(buffv, modificator);
    } else {
        modif = true;
        buffv = modificator;
    }
}

template <class T, class F>
void Treap<T, F>::Node::Update() {
    if (rev) {
        rev = false;
        std::swap(ll, rr);
        if (ll != nullptr) {
            ll->rev = (1 ^ ll->rev);
        }
        if (rr != nullptr) {
            rr->rev = (1 ^ rr->rev);
        }
    }
    if (modif) {
        modif = false;
        if (ll != nullptr) {
            ll->ModifyUpdater(buffv);
        }
        if (rr != nullptr) {
            rr->ModifyUpdater(buffv);
        }
        val = func(val, buffv);
    }
}

template <class T, class F>
void Treap<T, F>::Node::UpdateSiz() {
    siz = 1;
    if (ll != nullptr) {
        siz += ll->siz;
    }
    if (rr != nullptr) {
        siz += rr->siz;
    }
}

template <class T, class F>
Treap<T, F>::Node::Node(T value, std::mt19937 *random, F funct) :
        rnd(random), priority((*rnd)() & 2147483648), siz(1), val(value), ll(nullptr),
        rr(nullptr), rev(false), func(funct), modif(false) {
}

template <class T, class F>
void Treap<T, F>::Node::Reverse(Treap<T, F>::Node *&t_, int l, int r) {
    t_->Update();
    if (r <= l) {
        return;
    }
    Node* tl = nullptr;
    Node* tm = nullptr;
    Node* tr = nullptr;
    Split(t_, t_, tr, r);
    Split(t_, tl, tm, l);
    if (tm != nullptr) {
        tm->rev = (1 ^ tm->rev);
    }
    Merge(t_, tl, tm);
    Merge(t_, t_, tr);
}

template <class T, class F>
void Treap<T, F>::Node::Modify(Treap<T, F>::Node *&t_, int l, int r, T modif) {
    t_->Update();
    if (r <= l) {
        return;
    }
    Node* tl = nullptr;
    Node* tm = nullptr;
    Node* tr = nullptr;
    Split(t_, t_, tr, r);
    Split(t_, tl, tm, l);
    if (tm != nullptr) {
        tm->ModifyUpdater(modif);
    }
    Merge(t_, tl, tm);
    Merge(t_, t_, tr);
}

template <class T, class F>
T* Treap<T, F>::Node::Get(size_t ind) {
    Update();
    if (ind == (ll == nullptr ? 0 : ll->siz)) {
        return &val;
    }
    if (ind < (ll == nullptr ? 0 : ll->siz)) {
        if (ll == nullptr) {
            return nullptr;
        }
        return ll->Get(ind);
    }
    if (rr == nullptr) {
        return nullptr;
    }
    return rr->Get(ind - (ll == nullptr ? 0 : ll->siz) - 1);
}

template <class T, class F>
void Treap<T, F>::Node::Merge(Treap<T, F>::Node *&rez, Treap<T, F>::Node *ll, Treap<T, F>::Node *rr) {
    if (ll != nullptr) {
        ll->Update();
    }
    if (rr != nullptr) {
        rr->Update();
    }
    if (ll == nullptr || rr == nullptr) {
        rez = (rr == nullptr ? ll : rr);
    } else if (ll->priority < rr->priority) {
        Merge(rr->ll, ll, rr->ll);
        rez = rr;
    } else {
        Merge(ll->rr, ll->rr, rr);
        rez = ll;
    }
    rez->UpdateSiz();
}

template <class T, class F>
void Treap<T, F>::Node::Split(Treap<T, F>::Node *what, Treap<T, F>::Node *&ll, Treap<T, F>::Node *&rr, int ls) {
    if (what == nullptr) {
        ll = nullptr;
        rr = nullptr;
        return;
    }
    what->Update();
    if ((what->ll != nullptr ? what->ll->siz : 0) >= ls) {
        Split(what->ll, ll, what->ll, ls);
        rr = what;
    } else {
        Split(what->rr, what->rr, rr, ls - (what->ll != nullptr ? what->ll->siz : 0) - 1);
        ll = what;
    }
    what->UpdateSiz();
}

template <class T, class F>
Treap<T, F>::Treap(const std::vector<T> &data, F funct, bool random, int seed) :
        root(nullptr), func(funct), size(data.size()),
        rnd(random ? (unsigned)std::chrono::steady_clock::now().time_since_epoch().count() : seed) {
    if (size > 0) {
        root = new Node(data[0], &rnd, func);
    }
    for (size_t i = 1; i < size; ++i) {
        Node* t = new Node(data[i], &rnd, func);
        root->Merge(root, root, t);
    }
}

template <class T, class F>
Treap<T, F>::Treap(Treap &&treap) : root(treap.root), func(treap.func), rnd(treap.rnd), size(treap.size) {
    treap.root = nullptr;
    treap.size = 0;
}

template <class T, class F>
Treap<T, F>& Treap<T, F>::operator=(Treap &&treap) {
    delete root;
    root = treap.root;
    func = treap.func;
    rnd = treap.rnd;
    size = treap.size;
    treap.root = nullptr;
    treap.size = 0;
}

template <class T, class F>
void Treap<T, F>::Reverse(size_t l, size_t r) {
    l = std::max(l, size_t(0));
    r = std::min(r, size);
    if (l >= r) {
        return;
    }
    if (root != nullptr) {
        root->Reverse(root, l, r);
    }
}

template <class T, class F>
void Treap<T, F>::Modify(T modif, size_t l, size_t r) {
    l = std::max(l, size_t(0));
    r = std::min(r, size);
    if (l >= r) {
        return;
    }
    if (root != nullptr) {
        root->Modify(root, l, r, modif);
    }
}

template <class T, class F>
T Treap<T, F>::GetAt(size_t ind) {
    T* rez = root->Get(ind);
    if (rez == nullptr) {
        throw std::length_error("Outside of memory");
    }
    return T(*rez);
}
